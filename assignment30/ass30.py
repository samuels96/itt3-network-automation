'''
 Program name      : ass30.py
 Author            : Samuel Seman
 Date created      : 31.9.2019
 Purpose           : Program made to fulfill the requirements
                     of the network automation assignment 30
'''

from jnpr.junos import Device
from jnpr.junos.op.routes import RouteTable
from jnpr.junos.utils.config import Config
from lxml import etree
import sys
import sqlite3

class NetworkAutomator:
    '''
    NetworkAutomator is class made specifically for network automation
    exercises. It contains related assingment objects and methods for
    using them for exercise specific tasks
    Args:
        hostname (str) - hostname of the junos device
        username (str) - username of the junos device
        psasword (str) - password of the provided junos device user
        dbname   (str) - name for sqlite database where data from junos
        device is stored
    '''
    
    def __init__(self, hostname, username, password):
        self.dev = Device(host=hostname, user=username, password=password)
        self.dev.open()
        self.routingTable = None

    def __del__(self):
        self.close_connection()
        
    def __padded_print(self, header = False, *args):
        args = [str(i) for i in args]
        if header is True:
            args = ['IP ADDRESS', 'PROTOCOL', 'VIA',
                    'AGE', 'NEXTHOP']
        
        while(len(args) < 5):
            args.append('')

        print("{:{align}{width}}{:{align}{width}}{:{align}{width}}{:{align}{width}}{:{align}{width}}"
        .format(*args, align = '<', width =  '20'))
        print('-'*100)
        
    def print_routingTable(self):
        '''
        padded print of the routing table
        '''
        if(self.has_routingTable() is False):
            print('Routing table not populated\n')
            return False

        valList = [dict(v) for v in self.routingTable.values()]

        self.__padded_print(header = True)
        for idx, k in enumerate(self.routingTable.keys()):
            self.__padded_print(k, valList[idx]['protocol'], valList[idx]['via'],
            valList[idx]['age'], valList[idx]['nexthop'])


    def has_routingTable(self):
        return self.routingTable is not None

    def update_routingTable(self):
        '''
        pulls RouteTable object from the junos network device
        and saves it as a class attribute
        '''
        self.routingTable = RouteTable(self.dev)
        self.routingTable.get()

    def close_connection(self):
        self.dev.close()

    def add_interface(self, interface, ipAdd, mask):
        pass

    def commit_rescue_conf(self):
        with Config(self.dev, mode='exclusive') as cu:
            rescue = cu.rescue(action="reload")
            if rescue is False:
                print ("No existing rescue configuration.")
            else:
                cu.pdiff()
                cu.commit()

    def commit_rollback_conf(self):
        with Config(self.dev, mode='exclusive') as cu:
           cu.rollback(rb_id=rollback_id)
           cu.pdiff()
           cu.commit()

    def commit_conf(self, confFile):
        with Config(self.dev, mode='exclusive') as cu:
            cu.load(path=confFile, merge=True)
            
            if cu.commit_check() is True:
                cu.commit()
                print('Configuration commited succesfully')
            else:
                print('Configuration commit fail')

    def save_active_conf_xml(self):
        config = self.dev.rpc.get_config()
        config = etree.tostring(config, encoding='unicode', pretty_print=True)

        with open("active.xml", "w") as text_file:
            text_file.write(config)

        print('Active configuration saved to active.xml\n')


    def save_active_conf_txt(self):
        config = str(self.dev.cli("show configuration"))

        with open("active.txt", "w") as text_file:
            text_file.write(config)

        print('Active configuration saved to active.cfg\n')

    def export_routing_table(self, dbname):
        '''
        Creates new database and adds table called routing_table 
        in sqlite database if none is existing and inserts available 
        routeTable into the newly created table
        args:
            dbname (str) - name of the database to update/create
        '''
        con = sqlite3.connect(dbname)
        if(self.has_routingTable() is False):
            print('Routing table not populated\n')
            return False

        tableName = 'routing_table'
        con.execute('''CREATE TABLE IF NOT EXISTS {} (id integer PRIMARY KEY,
                    ip_address text, protocol text, via text, age integer,
                    nexthop text)'''.format(tableName))
                       
        cur = con.cursor() 
        id = 1
        for ip_add,info in self.routingTable.items():
            cur.execute("""INSERT or REPLACE INTO routing_table 
                    (id, ip_address, protocol, via, age, nexthop) VALUES 
                    (?, ?, ?, ?, ?, ?)""", (id, ip_add, info[0][1], info[1][1], 
                    info[2][1], info[3][1]))
            id += 1

        con.commit()
        print('Table {} updated\n'.format(tableName))

def main():
    ASS29 = NetworkAutomator('192.168.10.101', 'root', 'Juniper1')

    if sys.argv[1] is '1':
        ASS29.save_active_conf_xml()
    elif sys.argv[1] is '2':
        ASS29.commit_conf('active.xml')

    ASS29.close_connection()

if __name__ == '__main__':
    main()
