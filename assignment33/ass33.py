import paramiko, time, os
from Tkinter import *
import tkMessageBox

class Ass33:
    def __init__(self, hostname, username, password):
        self.hostname = hostname
        self.username = username
        self.password = password
        self.outputBuffer = None

        self.sshClient = paramiko.SSHClient()
        self.sshClient.set_missing_host_key_policy(paramiko.AutoAddPolicy())

        self.sshChannel = None
        self.cliMode = False
    
    def ERROR_ssh_not_established(self):
        print('SSH connection not established, invoke ssh_connect first')

    def ERROR_cli_mode(self):
        print('SSH connection not established, invoke ssh_connect first')

    def ssh_connect(self):
        self.sshClient.connect(hostname = self.hostname, username = self.username, password = self.password)
        self.sshChannel = self.sshClient.invoke_shell()
        time.sleep(1)

    def enter_cli(self):
        if self.sshChannel is None:
            self.ERROR_ssh_not_established()
            return
        self.sshChannel.send('cli\n')
        self.cliMode = True
        time.sleep(1)

    def get_show_configuration(self, bytesToRecv = 10000):
        if self.sshChannel is None:
            self.ERROR_ssh_not_established()
            return
        if self.cliMode is False:
            self.ERROR_cli_mode()
            return

        self.sshChannel.send('show configuration | no-more\n')
        time.sleep(1)
        self.outputBuffer = self.sshChannel.recv(bytesToRecv).decode('utf-8')

    def save_outputBuffer_to_file(self):
        with open('output.txt', 'w') as f:
            f.write(self.outputBuffer)

    def print_outputBuffer(self):
        print(self.outputBuffer)

    def ssh_close(self):
        self.sshChannel.close()
        self.sshChannel = None

class GUI:
    def __init__(self, title):
        self.root = Tk(className = title)

        self.labels = list()
        self.entries = list()
        self.buttons = list()

        self.sshObj = None
        self.sshHostname = None
        self.sshUsername = None
        self.sshPassword = None

        self.currentFrame = 0
        self.__init_frame1()

    def __init_frame1(self):
        self.root.geometry('400x200')
        self.add_frame1_widgets()
        self.currentFrame = 1

    def __init_frame2(self):
        self.add_frame2_widgets()
        self.currentFrame = 2 

    def __empty_widget_list(self):
        self.labels = list()
        self.entries = list()
        self.buttons = list()

    def __destroy_frame(self):
        for widget in self.root.winfo_children():
            widget.destroy()
        self.__empty_widget_list()

    def __get_ssh_data(self):
        self.sshHostname = self.entries[0].get()
        self.sshUsername = self.entries[1].get()
        self.sshPassword = self.entries[2].get()

    def __establish_ssh_connection(self):
        self.__get_ssh_data()

        self.sshObj = Ass33(self.sshHostname, self.sshUsername, self.sshPassword)
        try:
            self.sshObj.ssh_connect()
            self.__destroy_frame()
            self.__init_frame2()
        except:
            tkMessageBox.showerror("Error", "Unable to establish SSH connection")
            return 1

    def __ssh_cli(self):
        try:
            self.sshObj.enter_cli()
            tkMessageBox.showinfo("Information", "CLI entered")
        except:
            tkMessageBox.showerror("Error", "Error entering CLI")

    def __ssh_get_configuration(self):
        try:
            self.sshObj.get_show_configuration()
            tkMessageBox.showinfo("Information", "Running confiugration loaded")
        except:
            tkMessageBox.showerror("Error", "Error retrieving configuration, make sure to enter CLI beforehand")

    def __ssh_save_configuration(self):
        try:
            self.sshObj.save_outputBuffer_to_file()
            tkMessageBox.showinfo("Information", "Running confiugration saved")
        except:
            tkMessageBox.showerror("Error", "Error saving configuration, make sure to retrieve configuration beforehand")


    def add_label(self, labelText):
        label = Label(self.root, text = labelText, font=('Arial, 10'))
        label.pack(fill = BOTH, expand=1)
        self.labels.append(label)

    def add_entry(self, text):
        entry = Entry(self.root) 
        entry.pack(fill = BOTH, expand=1)
        self.entries.append(entry)
        entry.insert('1', text)

    def add_button(self, text, func):
        button = Button(self.root, text=text, command = func)
        button.pack(fill = BOTH, expand=1)
        self.buttons.append(button)

    def add_frame1_widgets(self):
        self.add_label('Hostname')
        self.add_entry('192.168.11.1')

        self.add_label('Username')
        self.add_entry('root')

        self.add_label('Password')
        self.add_entry('Juniper1')

        self.add_button('Establish SSH connection', self.__establish_ssh_connection)

    def add_frame2_widgets(self):
        self.add_label("Enter cli and load the configuration before saving it")
        self.add_button('Enter cli', self.__ssh_cli)
        self.add_button('Load configuration', self.__ssh_get_configuration)
        self.add_button('Save configuration to file', self.__ssh_save_configuration)

    def mainloop(self):
        self.root.mainloop()

def main():
    g = GUI('Assignment 33')
    g.mainloop()

def assClass():
   assClass = Ass33('192.168.11.1', 'root', 'Juniper1')
   assClass.ssh_connect()
   assClass.enter_cli()
   assClass.get_show_configuration()
   assClass.print_outputBuffer()
   assClass.save_outputBuffer_to_file()

main()
